import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <p>
            Exit Well co. <br/>
            잘나가는회사
          </p>
        </header>
        <body>
          <div className="Half-div">
              <p>
                hi there!
              </p>
          </div>
        </body>
      </div>
    );
  }
}

export default App;
